# Prodigious Front-end Challenge
Below you can find the solution of the proposed test, regarding the recruiting area instructions.

## Required dependencies

`node.js`
`gulp`

## Required actions

Once you verify you have node and gulp installed please proceed to run  `npm install` on the project folder in your terminal.


## Gulp tasks

`gulp` Use this task to run the project in an local server *http://localhost:3000* .

`gulp lint` Use this task to run the ESLint tool over the codebase.

`gulp test` Use this task to run an verify the jasmine test.

`gulp TDD` Use this task to turn on a tdd environment.

## Validations

`CSS` https://jigsaw.w3.org/css-validator/

`HTML` https://validator.w3.org/

`JS` ESLint

Best Regards :: James Garcia

## Template literals approach

I created the branch called `changeSet/templateLiterals` for you to validate a refactored version using the ES6 template literals feature as approach.